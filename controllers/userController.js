const User = require('../models/user');
const Credential = require('../models/credentials');

module.exports.getUserInfo = (req, res) => {
  Credential.findById(req.user._id).exec()
  .then(user => {

    if (!user) {
      return res.status(400).json({message: "No user found"});
    }
    const userProfileInfo = new User({_id: req.user._id, username: req.user.username, createdDate: new Date().toISOString()})
    res.json({user: userProfileInfo});
  })
  .catch(err => {
    res.status(500).json({message: err.message});
  })
  
}

module.exports.deleteUserProfile = (req, res) => {
  Credential.findByIdAndDelete(req.user._id).exec()
  .then(() => {
    res.json({message: "Success"});
  })
  .catch(err => {
    res.status(500).json({message: err.message});
  })
}

module.exports.changeUserPassword = (req, res) => {
  if (req.body.oldPassword != req.user.password) {
    return res.status(400).json({message: "Wrong old password"});
  }

  Credential.updateOne({username: req.user.username}, {$set: {password: req.body.newPassword}})
  .then(() => {
    res.json({message: "Success"});
  })
  .catch(err => {
    res.status(500).json({message: err.message});
  })
}