const Note = require('../models/note');

module.exports.addNote = (req, res) => {
  const { text } = req.body;

  if (!text) {
    return res.status(400).json({message: "No title text found"});
  }
  
  const note = new Note({ userId: req.user._id, completed: false, text, createdDate: new Date().toISOString()});
  note.save()
      .then(() => {
        res.json({message: "Success"})
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}

module.exports.getNote = (req, res) => {

  Note.find({}).exec()
      .then(notes => {
        if (!notes) {
          return res.status(400).json({message: "No notes found"});
        }
        res.json({notes});
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}

module.exports.getNoteById = (req, res) => {

  Note.findById(req.params.id).exec()
      .then(note => {
        if (!note) {
          return res.status(400).json({message: "No note found"});
        }
        res.json({note});
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}

module.exports.updateNoteById = (req, res) => {

  Note.findByIdAndUpdate(req.params.id, {$set: req.body}).exec()
      .then(() => {
        if (!req.body.text) {
          return res.status(400).json({message: "No note found"});
        }
        res.json({message: "Success"});
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}

module.exports.updateCompletedNoteById = (req, res) => {

  Note.findById(req.params.id).exec()
      .then(note => {    
        Note.findByIdAndUpdate(req.params.id, {completed: !note.completed}).exec()
            .then(() => {
              res.json({message: "Success"});
            })
            .catch(err => {
              res.status(500).json({message: err.message});
            })
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}

module.exports.deleteNoteById = (req, res) => {

  Note.findByIdAndDelete(req.params.id).exec()
  .then(note => {
    if (!note) {
      return res.status(400).json({message: "No note found"});
    }
    res.json({message: "Success"});
  })
  .catch(err => {
    res.status(500).json({message:err.message});
  })
}