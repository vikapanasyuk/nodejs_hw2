const jwt = require('jsonwebtoken');
const User = require('../models/credentials');

const { secret } = require('../config/auth');

module.exports.register = (req, res) => {

  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({message: "Login or password are missed"});
  }

  const user = new User({username, password});
  user.save()
      .then(() => {
        res.json({message: "Success"})
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}

module.exports.login = (req, res) => {

  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({message: "Login or password are missed"});
  }

  User.findOne({ username, password }).exec()
      .then(user => {
        if (!user) {
          return res.status(400).json({message: "No user with such username or password found"});
        }
        res.json({jwt_token: jwt.sign(JSON.stringify(user), secret)});
      })
      .catch(err => {
        res.status(500).json({message: err.message});
      })
}