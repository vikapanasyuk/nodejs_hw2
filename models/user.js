const mongoose = require('mongoose');

module.exports = mongoose.model('user', {
  _id: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  createdDate: {
    type: String,
    required: true
  }
});