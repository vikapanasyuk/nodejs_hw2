const mongoose = require('mongoose');

module.exports = mongoose.model('error', {
  message: String
});