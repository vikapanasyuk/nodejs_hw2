const mongoose = require('mongoose');

module.exports = mongoose.model('note', {
  userId: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  createdDate: {
    type: String,
    required: true
  }
});