const mongoose = require('mongoose');

module.exports = mongoose.model('credential', {
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});