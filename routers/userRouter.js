const express = require('express');
const router = express.Router();

const { getUserInfo, deleteUserProfile, changeUserPassword} = require('../controllers/userController');

const authMiddleware = require('../middlewares/authMiddleware');


router.get('/users/me', authMiddleware, getUserInfo);
router.delete('/users/me', authMiddleware, deleteUserProfile);
router.patch('/users/me', authMiddleware, changeUserPassword);

module.exports = router;