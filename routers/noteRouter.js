const express = require('express');
const router = express.Router();

const { addNote, getNote, getNoteById, updateNoteById, updateCompletedNoteById, deleteNoteById } = require('../controllers/noteController');

const authMiddleware = require('../middlewares/authMiddleware');

router.post('/notes', authMiddleware, addNote);
router.get('/notes', authMiddleware, getNote);
router.get('/notes/:id', authMiddleware, getNoteById);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, updateCompletedNoteById);
router.delete('/notes/:id', authMiddleware, deleteNoteById);

module.exports = router;